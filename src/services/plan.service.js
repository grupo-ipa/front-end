import axios from 'axios';

const api = axios.create({
  baseURL: process.env.REACT_APP_API_URL
});

export const fetchCurrentBeersInProduction = async () => {
  const { data } = await api.get('produto');
  return data;
}

export const fetchBeerInfo = async (id) => {
  console.log(id);
  console.log(`produto/${id}`);
  const r = await api.get(`produto/${id}`);
  // const { data } = await api.get(`produto/${id}`);
  // console.log(data);
  // return data;
  console.log(r);
  console.log(r.data);
  return r.data;
}

export const createNewBeerProduction = async (name) => {
  const { data } = await api.post('produto', { nome: name });
  return data;
}

export const createNewActivity = async (id, descricao, dataAtividade, dataLimite) => {
  const { data } = await api.post(`atividades/${id}`, { dataAtividade, dataLimite, descricao, status: 0 });
  return data
}

export const fetchActivityByIdAndDate = async (id, date) => {
  const { data } = await api.get(`atividades/${id}`, { params: { data: date } });
  return data;
}

export const addBeerToTank = async (tankId, produtoId) => {
  const { data } = await api.put(`tanque/${tankId}`, { produtoId });
  return data
}

// todo: idProduct is rendundanct here
export const updateActivityStatus = async (idActivity, idProduct, status) => {
  const { data } = await api.put(`atividades/${idProduct}/${idActivity}`, { status });
  return data;
}

export const deleteProduction = async (id) => {
  const { status } = await api.delete(`produto/${id}`);
  return status;
}

export const deleteActivity = async (id) => {
  const { data } = await api.delete(`atividades/${id}`);
  return data;
}