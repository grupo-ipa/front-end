import axios from 'axios';

const api = axios.create({
  baseURL: process.env.REACT_APP_API_URL
});

export const createBarrel = async (number) => {
  const id = +number;
  const { status } = await api.post('tanque', { id });
  return status;
}

export const fetchBarrelsStatus = async () => {
  const { data } = await api.get('tanque');
  return data;
}

export const fetchLastUpdates = async () => {
  const { data } = await api.get('ultimasAtividades');
  return data;
}

export const fetchNextActivities = async (productId) => {
  const { data } = await api.get(`proximasAtividades/${productId}`);
  return data;
}

export const fetchBarrelInfo = async (barrelNumber) => {
  const { data } = await api.get(`tanque/${barrelNumber}`);
  return data.pop();
}

export const fetchAttrHistoryByProduct = async (tankId, atributo) => {
  const { data } = await api.get(`historico/${tankId}`, { params: { atributo } });
  return data;
}

export const addNewEntryToProp = async (produtoId, tanqueId, atributo, valor) => {
  let response;
  switch (atributo) {
    case "dataProducao":
    case "previsaoEnvase":
    case "envaseReal":
      response = await api.put(`produto/${produtoId}`, { [atributo]: valor });
      break;
    default:
      response = await api.post('historico', { produtoId, tanqueId, atributo, valor, nome: 'admin' });
  }
  return response.data;
}