import React from 'react';
import LoginForm from '../../../components/LoginForm/login-form.component';
import './login.styles.scss';
// import backgroundImage from '../../../assets/images/beerbg.jpg';
import backgroundImage from '../../../assets/images/ekaut.jpg';
import { Redirect } from 'react-router-dom';
import { isAuthenticated } from '../../../utils/functions/auth';

const LoginPage = () => {
  if (isAuthenticated()) {
    return <Redirect to="/dashboard" />;
  }
  return (
    <div className="login-page-wrapper hero is-fullheight">
      <div className="columns hero-body">
        <div className="column is-one-third">
          <LoginForm />
        </div>
        <div className="column is-two-thirds hero is-fullheight">
          <div className="hero-body">
            <img className="image-background" src={backgroundImage} alt="" />
          </div>
        </div>
      </div>
    </div>
  );
};

export default LoginPage;
