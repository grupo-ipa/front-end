import React from 'react';
import ReportsContainer from '../../../components/ReportsContainer/reports-container.component';
// import './schedule.styles.scss';

class ReportsPage extends React.Component {
  selectBeerPlanning = (id, name) => {
    this.props.history.push({
      pathname: `/cronograma/${id}`,
      state: { custom: [name] },
    });
  };

  render() {
    return (
      <section className="section app-section hero">
        <div className="hero-head">
          <div className="column">
            <h2 className="title schedule-page__title">Seleção de Tonel</h2>
          </div>
        </div>
        <div className="hero-body">
          <ReportsContainer />
        </div>
      </section>
    );
  }
}

export default ReportsPage;
