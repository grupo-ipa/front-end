import React from 'react';
import { useHistory } from 'react-router-dom';

import { createBarrel } from '../../../services/dashboard.service';
import { logout } from '../../../utils/functions/auth';

import './settings.styles.scss';

const SettingsPage = () => {
  const [modal, setModal] = React.useState(false);
  const [loading, setLoading] = React.useState(false);
  const history = useHistory();

  let number;

  const logoutUser = () => {
    logout();
    history.push('/login');
  };

  const handleChange = ({ target: { value } }) => (number = value);

  const handleSubmit = async (event) => {
    event.preventDefault();
    setLoading(true);
    await createBarrel(number);
    setModal(false);
    setLoading(false);
    history.push(`dashboard/${number}`);
  };

  const modalComponent = () => (
    <div className="modal is-active">
      <div className="modal-background"></div>
      <div className="modal-card custom-card">
        <header className="modal-card-head">
          <p className="modal-card-title">Cadastrar novo tanque</p>
          <button
            className="delete"
            aria-label="close"
            onClick={() => setModal(false)}
          ></button>
        </header>
        <section className="modal-card-body">
          <form noValidate>
            <div className="field is-horizontal">
              <div className="field-label is-normal">
                <label className="label">Número</label>
              </div>
              <div className="field-body">
                <div className="field">
                  <p className="control">
                    <input
                      className="input"
                      type="text"
                      placeholder="Número"
                      onChange={handleChange}
                    />
                  </p>
                </div>
              </div>
            </div>

            <div className="field is-grouped is-grouped-centered">
              <button
                className={`button is-success ${loading ? 'is-loading' : ''}`}
                onClick={handleSubmit}
              >
                Criar
              </button>
            </div>
          </form>
        </section>
      </div>
    </div>
  );

  return (
    <section className="section app-section hero">
      {modal && modalComponent()}
      <div className="hero-head">
        <div className="column">
          <h2 className="title settings-page__title">Configurações</h2>
        </div>
      </div>
      <div className="hero-body">
        <section className="section app-section">
          <div className="columns is-multiline is-mobile">
            <div className="column is-half is-offset-one-quarter settings-page__option">
              <button
                className="button is-default"
                onClick={() => setModal(true)}
              >
                Adicionar novo tanque
              </button>
            </div>
            <div className="column is-half is-offset-one-quarter settings-page__option">
              <button
                className="button is-default"
                onClick={() => logoutUser()}
              >
                Mudar Usuário
              </button>
            </div>
            <div className="column is-half is-offset-one-quarter settings-page__option">
              <button className="button is-default">Sobre</button>
            </div>
          </div>
        </section>
      </div>
    </section>
  );
};
export default SettingsPage;
