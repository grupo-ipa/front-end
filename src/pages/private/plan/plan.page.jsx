import React from 'react';

import PlanContainer from '../../../components/PlanContainer/plan-container.component';
import './plan.styles.scss';
import { deleteProduction } from '../../../services/plan.service';

class PlanPage extends React.Component {
  constructor(props) {
    super(props);
    this.id = props.match.params.id;
    const date = new Date();
    this.months = new Array(12)
      .fill(1)
      .map((c, i) => (c + i >= 10 ? `${c + i}` : `0${c + i}`));
    this.years = new Array(4).fill(date.getFullYear()).map((c, i) => c + i);
    this.state = {
      isModalOpened: false,
      monthNumber: date.getMonth(),
      month: this.months[date.getMonth()],
      year: this.years[0],
    };
  }

  selectMonth = ({ target: { value } }) =>
    this.setState({ month: value, monthNumber: +value - 1 });

  selectYear = ({ target: { value } }) => this.setState({ year: value });

  monthSelector = () => (
    <div className="select">
      <select onChange={this.selectMonth} value={this.state.month}>
        {this.months.map((month) => (
          <option key={month}>{month}</option>
        ))}
      </select>
    </div>
  );

  yearSelector = () => (
    <div className="select">
      <select onChange={this.selectYear} value={this.state.year}>
        {this.years.map((year) => (
          <option key={year}>{year}</option>
        ))}
      </select>
    </div>
  );

  openModal = () => this.setState({ isModalOpened: true });

  closeModal = () => this.setState({ isModalOpened: false });

  delete = async () => {
    const resultStatus = await deleteProduction(this.id);
    if (resultStatus === 201) {
      this.closeModal();
      this.props.history.push('/cronograma');
    } else {
      //handle error
    }
  };

  displayModal = () => {
    if (this.state.isModalOpened) {
      return (
        <div className="modal is-active">
          <div className="modal-background"></div>
          <div className="modal-card custom-card">
            <header className="modal-card-head">
              <p className="modal-card-title">Deletar produção</p>
              <button
                className="delete"
                aria-label="close"
                onClick={this.closeModal}
              ></button>
            </header>
            <section className="modal-card-body">
              <p>
                Tem certeza que deseja deletar esta produção? Esta ação é
                <strong> definitiva</strong>.
              </p>
            </section>
            <footer
              style={{ display: 'flex', justifyContent: 'center' }}
              className="modal-card-foot"
            >
              <button className="button is-danger" onClick={this.delete}>
                Deletar Produção
              </button>
            </footer>
          </div>
        </div>
      );
    }
  };

  render() {
    return (
      <section className="section app-section hero plan-page-wrapper">
        {this.displayModal()}
        <div className="hero-head">
          <p>Mês</p>
          {this.monthSelector()}
          <p>Ano</p>
          {this.yearSelector()}
          <button
            style={{ marginLeft: '12px' }}
            className="button is-default is-rounded"
            onClick={this.openModal}
          >
            Deletar Cerveja
          </button>
        </div>
        <div className="hero-body">
          <PlanContainer
            month={this.state.monthNumber}
            year={this.state.year}
            beerId={this.id}
          />
        </div>
      </section>
    );
  }
}

export default PlanPage;
