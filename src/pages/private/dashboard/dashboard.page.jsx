import React from 'react';
import { withRouter } from 'react-router-dom';
import DashboardContainer from '../../../components/DashboardContainer/dashboard-container.component';
import Slider from '../../../components/Slider/slider.component';
import './dashboard.styles.scss';

class DashboardPage extends React.Component {
  constructor(props) {
    super(props);
    let { id } = props.match.params;
    if (!id) {
      id = 1;
      props.history.replace('/dashboard/1');
    }
    this.state = {
      barrel: +id,
      currentView: 'main',
    };
  }

  changeView = (page) => this.setState({ currentView: page });

  selectCard = (number) => {
    this.setState({ barrel: number, currentView: 'main' });
    this.props.history.push(`/dashboard/${number}`);
  };

  render() {
    return (
      <section className="section app-section hero">
        <div className="hero-head">
          <Slider
            selectCard={this.selectCard}
            currentPage={this.state.barrel}
          />
        </div>
        <div className="hero-body">
          <DashboardContainer
            changeView={this.changeView}
            currentView={this.state.currentView}
            currentBarrel={this.state.barrel}
          />
        </div>
      </section>
    );
  }
}

export default withRouter(DashboardPage);
