import React from 'react';
import { fetchBarrelsStatus } from '../../../services/dashboard.service';
import {
  addBeerToTank,
  createNewBeerProduction,
} from '../../../services/plan.service';
import Spinner from '../../../components/Spinner/spinner.component';
import './assign-barrel.styles.scss';

class AssignBarrel extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      barrels: [],
      isModalOpened: false,
      beerName: '',
    };
  }

  componentDidMount = async () => {
    if (!this.props.location.state || !this.props.location.state.beerName) {
      this.props.history.push('/cronograma');
      return;
    }
    this.setState({ loading: true });
    const barrels = await fetchBarrelsStatus();
    this.setState({ barrels, beerName: this.props.location.state.beerName });
    this.setState({ loading: false });
  };

  openModal = () => this.setState({ isModalOpened: true });

  closeModal = () => this.setState({ isModalOpened: false });

  handleSubmit = async ({ target: { innerText } }) => {
    const tankId = +innerText;
    const selectedBarrel = this.state.barrels.find(({ id }) => id === tankId);
    if (!selectedBarrel.produtoId) {
      const { id } = await createNewBeerProduction(this.state.beerName);
      await addBeerToTank(tankId, id);
      this.props.history.push(`/dashboard/${tankId}`);
    }
  };

  render() {
    if (this.state.loading) {
      return <Spinner />;
    }
    return (
      <section className="section app-section">
        {this.state.isModalOpened && <p>modal</p>}
        <div className="columns is-multiline is-mobile">
          {this.state.barrels.map(({ id, produtoId }) => (
            <div
              key={id}
              className={`column is-one-quarter barrel-option ${
                produtoId ? 'barrel-occupied' : 'barrel-empty'
              }`}
              onClick={this.handleSubmit}
              disabled={!!produtoId}
            >
              {id}
            </div>
          ))}
        </div>
      </section>
    );
  }
}

export default AssignBarrel;
