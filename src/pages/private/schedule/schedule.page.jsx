import React from 'react';
import ScheduleContainer from '../../../components/ScheduleContainer/schedule-container.component';
import './schedule.styles.scss';

class SchedulePage extends React.Component {
  selectBeerPlanning = (id, name) => {
    this.props.history.push({
      pathname: `/cronograma/${id}`,
      state: { custom: [name] },
    });
  };

  render() {
    return (
      <section className="section app-section hero">
        <div className="hero-head">
          <div className="column">
            <h2 className="title schedule-page__title">
              Produtos
              <br />
              Cadastrados
            </h2>
          </div>
        </div>
        <div className="hero-body">
          <ScheduleContainer selectBeer={this.selectBeerPlanning} />
        </div>
      </section>
    );
  }
}

export default SchedulePage;
