import React from 'react';
import { Redirect } from 'react-router-dom';
import PrivateRoute from '../../../utils/components/private-route.component';
import SideMenu from '../../../components/SideMenu/side-menu.component';
import DashboardPage from '../dashboard/dashboard.page';
import ReportsPage from '../reports/reports.page';
import SchedulePage from '../schedule/schedule.page';
import PlanPage from '../plan/plan.page';
import SettingsPage from '../settings/settings.page';
import Breadcrumb from '../../../components/Breadcrumb/breadcrumb.component';

import './private-container.styles.scss';
import AssignBarrel from '../assign-barrel/assign-barrel.page';

const PrivateContainerPage = () => (
  <div className="main-wrapper hero is-fullheight">
    <div className="columns hero-body">
      <div className="column is-one-fifth">
        <SideMenu />
      </div>
      <div className="column main-app hero">
        <div className="hero-head">
          <Breadcrumb />
          {/* <PrivateRoute path="/:path" component={Breadcrumb} /> */}
        </div>
        <div className="hero-body">
          <PrivateRoute exact path="/">
            <Redirect to="/dashboard" />
          </PrivateRoute>
          <PrivateRoute exact path="/dashboard" component={DashboardPage} />
          <PrivateRoute
            exact
            path="/dashboard/:id?"
            component={DashboardPage}
          />
          <PrivateRoute exact path="/relatorios" component={ReportsPage} />
          <PrivateRoute exact path="/cronograma" component={SchedulePage} />
          <PrivateRoute
            exact
            path="/selecionar-barril"
            component={AssignBarrel}
          />
          <PrivateRoute exact path="/cronograma/:id" component={PlanPage} />
          <PrivateRoute exact path="/configuracao" component={SettingsPage} />
        </div>
      </div>
    </div>
  </div>
);

export default PrivateContainerPage;
