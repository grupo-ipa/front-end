import React from 'react';
import { useHistory } from 'react-router-dom';

const SideMenuButton = (props) => {
  const history = useHistory();
  return (
    <button
      className={`button is-default ${props.active ? 'is-active' : ''}`}
      onClick={() => history.push(props.path)}
    >
      {props.text}
    </button>
  );
};
export default SideMenuButton;
