import React from 'react';
import { withRouter } from 'react-router-dom';
import './side-menu.styles.scss';
import logo from '../../assets/images/logo_escuro.png';
import { logout } from '../../utils/functions/auth';
import SideMenuButton from './SideMenuButton/side-menu-button.component';

const sideMenuButtons = [
  { text: 'Dashboard', path: '/dashboard' },
  { text: 'Relatórios', path: '/relatorios' },
  { text: 'Cronograma', path: '/cronograma' },
  { text: 'Configurações', path: '/configuracao' },
];

class SideMenu extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currentPage: props.history.location.pathname || '/',
    };
  }

  logout() {
    logout();
    this.props.history.push('/login');
  }

  render() {
    return (
      <section className="side-menu-wrapper hero is-fullheight">
        <div className="hero-head">
          <img className="side-menu--logo" src={logo} alt="" />
        </div>
        <div
          className="hero-body"
          style={{ alignItems: 'flex-start', marginTop: '70px' }}
        >
          <div className="side-menu__options">
            {sideMenuButtons.map((btn, i) => (
              <SideMenuButton
                key={i}
                active={this.props.history.location.pathname.startsWith(
                  btn.path
                )}
                {...btn}
              />
            ))}
          </div>
        </div>
        <div className="hero-foot side-menu__bottom">
          <button className="button is-default" onClick={() => this.logout()}>
            Sair
          </button>
        </div>
      </section>
    );
  }
}

export default withRouter(SideMenu);
