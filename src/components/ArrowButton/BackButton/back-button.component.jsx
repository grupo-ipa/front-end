import React from 'react';
import { useHistory } from 'react-router-dom';
import ArrowButton from '../arrow-button.component';

const BackButton = () => {
  const history = useHistory();
  return <ArrowButton text="&larr;" fn={history.goBack} />;
};

export default BackButton;
