import React from 'react';
import './arrow-button.styles.scss';

const ArrowButton = (props) => (
  <div className="arrow-button-wrapper" onClick={() => props.fn()}>
    <p>{props.text}</p>
  </div>
);

export default ArrowButton;
