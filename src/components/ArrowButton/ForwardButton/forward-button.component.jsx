import React from 'react';
import { useHistory } from 'react-router-dom';
import ArrowButton from '../arrow-button.component';

const ForwardButton = () => {
  const history = useHistory();
  return <ArrowButton text="&rarr;" fn={history.goForward} />;
};

export default ForwardButton;
