import React from 'react';
import { createNewActivity } from '../../../services/plan.service';

class CreateNewActivityModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      description: '',
      limitDate: '',
    };
  }

  handleChange = ({ target: { name, value } }) => {
    this.setState({ [name]: value });
  };

  handleSubmit = async (e) => {
    e.preventDefault();
    this.setState({ loading: true });
    const { description, limitDate } = this.state;
    await createNewActivity(
      this.props.id,
      description,
      this.props.date,
      limitDate
    );
    this.setState({ loading: false });
    this.props.closeModal();
  };

  render() {
    return (
      <div className="modal is-active">
        <div className="modal-background"></div>
        <div className="modal-card custom-card">
          <header className="modal-card-head">
            <p className="modal-card-title">
              Criar nova atividade na data:
              {` ${this.props.date}`}
            </p>
            <button
              className="delete"
              aria-label="close"
              onClick={this.props.closeModal}
            ></button>
          </header>
          <section className="modal-card-body">
            <div className="field is-horizontal">
              <div className="field-label is-normal">
                <label className="description">Descrição</label>
              </div>
              <div className="field-body">
                <div className="field">
                  <input
                    name="description"
                    className="input"
                    type="text"
                    placeholder="Descrição"
                    onChange={this.handleChange}
                  />
                </div>
              </div>
            </div>
            <div className="field is-horizontal">
              <div className="field-label is-normal">
                <label className="name">Data limite</label>
              </div>
              <div className="field-body">
                <div className="field">
                  <input
                    name="limitDate"
                    className="input"
                    type="date"
                    min={this.props.date}
                    onChange={this.handleChange}
                    pattern="\d{4}-\d{2}-\d{2}"
                  />
                </div>
              </div>
            </div>
          </section>

          <footer className="modal-card-foot is-centered">
            <button
              type="submit"
              className={`button is-success ${
                this.state.loading ? 'is-loading' : ''
              }`}
              onClick={this.handleSubmit}
            >
              Criar
            </button>
          </footer>
        </div>
      </div>
    );
  }
}

export default CreateNewActivityModal;
