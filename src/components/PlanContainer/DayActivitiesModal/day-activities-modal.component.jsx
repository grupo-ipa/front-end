import React from 'react';
import moment from 'moment';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import Spinner from '../../Spinner/spinner.component';

import {
  fetchActivityByIdAndDate,
  updateActivityStatus,
  deleteActivity,
} from '../../../services/plan.service';

class DayActivitiesModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      isModalOpened: false,
      activities: [],
      activitiesCheckbox: null,
      isDeleting: false,
    };
  }

  isDateSelectedBefore = () => {
    return moment().startOf('day').isSameOrBefore(this.props.date);
  };

  componentDidMount = async () => await this.loadContent();

  loadContent = async () => {
    const activities = await fetchActivityByIdAndDate(
      this.props.id,
      this.props.date
    );
    const activitiesCheckbox = new Map();
    activities.forEach(({ id, status }) => {
      status = +status;
      activitiesCheckbox.set(id, status);
    });
    this.setState({ loading: false, activities, activitiesCheckbox });
  };

  handleActivityStatusChange = async (
    id,
    produtoId,
    { target: { name, checked } }
  ) => {
    const result = await updateActivityStatus(id, produtoId, checked);
    if (result.pop()) {
      const activitiesCheckbox = this.state.activitiesCheckbox;
      activitiesCheckbox.set(+name, checked);
      this.setState({ activitiesCheckbox });
    }
  };

  deleteActivity = async (id) => {
    this.setState({ isDeleting: true });
    const response = await deleteActivity(id); // todo[vob] -> this response is not optimal. Ask backend to send the new list updated
    this.setState({ isDeleting: false });
    if (response === 'deleted') {
      await this.loadContent();
    }
  };

  displayModalContent = () => {
    if (this.state.loading) {
      return <Spinner />;
    }
    if (this.state.activities.length === 0) {
      return <p>Nenhuma atividade registrada neste dia.</p>;
    }
    return (
      <table className="table is-fullwidth has-text-centered">
        <thead className="has-text-centered">
          <tr>
            <th>Atividade</th>
            <th>Data Limite</th>
            <th>Status</th>
            <th>Ação</th>
          </tr>
        </thead>
        <tbody>
          {this.state.activities.map(
            ({ id, produtoId, descricao, dataLimite }) => (
              <tr key={id}>
                <td>{descricao}</td>
                <td>{dataLimite}</td>
                <td>
                  <input
                    name={id}
                    type="checkbox"
                    checked={this.state.activitiesCheckbox.get(id)}
                    onChange={(e) =>
                      this.handleActivityStatusChange(id, produtoId, e)
                    }
                  />
                </td>
                <td>
                  {this.state.isDeleting ? (
                    <div className="loader"></div>
                  ) : (
                    <span className="icon">
                      <FontAwesomeIcon
                        className="slider-control"
                        icon={faTrashAlt}
                        size="sm"
                        style={{ marginTop: -2 }}
                        onClick={() => this.deleteActivity(id)}
                      />
                    </span>
                  )}
                </td>
              </tr>
            )
          )}
        </tbody>
      </table>
    );
  };

  render() {
    return (
      <div className="modal is-active">
        <div className="modal-background"></div>
        <div className="modal-card custom-card">
          <header className="modal-card-head">
            <p className="modal-card-title">
              Data:
              {` ${this.props.date}`}
            </p>
            <button
              className="delete"
              aria-label="close"
              onClick={this.props.closeThisModal}
            ></button>
          </header>
          <section className="modal-card-body">
            {this.displayModalContent()}
          </section>
          {this.isDateSelectedBefore() && (
            <footer className="modal-card-foot is-centered">
              <button
                className="button is-default"
                onClick={this.props.openNewModal}
                disabled={this.state.modalLoading}
              >
                Adicionar Nova Atividade
              </button>
            </footer>
          )}
        </div>
      </div>
    );
  }
}

export default DayActivitiesModal;
