import React from 'react';
import moment from 'moment';

import DayActivitiesModal from './DayActivitiesModal/day-activities-modal.component';
import CreateNewActivityModal from './CreateNewActivityModal/create-new-activity-modal.component';

import { formatDay, formatDateToISO } from '../../utils/functions/date';

import './plan-container.styles.scss';

const days = [
  'Segunda',
  'Terça',
  'Quarta',
  'Quinta',
  'Sexta',
  'Sábado',
  'Domingo',
];

class PlanContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      modalLoading: false,
      isActivitiesListModalOpened: false,
      isCreateNewActivityModalOpened: false,
      daySelected: null,
      dayActivity: null,
      formattedDate: null,
    };
  }

  openActivitiesListModal = async ({ target: { textContent } }) => {
    this.setState({
      isActivitiesListModalOpened: true,
      formattedDate: formatDateToISO(
        textContent,
        this.props.month,
        this.props.year
      ),
    });
  };

  closeActivitiesListModal = () =>
    this.setState({ isActivitiesListModalOpened: false, daySelected: null });

  openCreateNewActivityModal = () => {
    this.setState({
      isActivitiesListModalOpened: false,
      isCreateNewActivityModalOpened: true,
    });
  };

  closeCreateNewActivityModal = () =>
    this.setState({
      isCreateNewActivityModalOpened: false,
      isActivitiesListModalOpened: true,
    });

  displayActivitiesModal = () =>
    this.state.isActivitiesListModalOpened && (
      <DayActivitiesModal
        date={this.state.formattedDate}
        closeThisModal={this.closeActivitiesListModal}
        openNewModal={this.openCreateNewActivityModal}
        id={this.props.beerId}
      />
    );

  displayCreateNewActivityModal = () =>
    this.state.isCreateNewActivityModalOpened && (
      <CreateNewActivityModal
        date={this.state.formattedDate}
        closeModal={this.closeCreateNewActivityModal}
        id={this.props.beerId}
      />
    );

  displayBody() {
    const month = moment()
      .year(this.props.year)
      .month(this.props.month)
      .date(1);
    const firstDayOfMonth = month.isoWeekday() - 1;
    const daysOnMonth = month.daysInMonth();
    const numberOfWeeks = Math.floor((daysOnMonth - 1) / 7) + 1;
    const monthDays = Array.from({ length: numberOfWeeks }, (_) =>
      Array(7).fill(0)
    );
    monthDays[0][firstDayOfMonth] = 1;
    for (let i = firstDayOfMonth + 1; i < daysOnMonth + firstDayOfMonth; ++i) {
      try {
        monthDays[Math.floor(i / 7)][i % 7] = i + 1 - firstDayOfMonth;
      } catch (_) {
        // gambeta (agosto de 2020 tem 6 semanas)
        monthDays.push([0, 0, 0, 0, 0, 0, 0]);
        monthDays[Math.floor(i / 7)][i % 7] = i + 1 - firstDayOfMonth;
      }
    }
    return (
      <table className="table is-fullwidth calendar has-text-centered">
        <thead>
          <tr>
            {days.map((day) => (
              <th className="calendar-header has-text-centered" key={day}>
                {day}
              </th>
            ))}
          </tr>
        </thead>
        <tbody>
          {monthDays.map((week, index) => (
            <tr key={index}>
              {week.map((day, dayIndex) => (
                <td
                  className={`${
                    day > 0 ? 'is-clickable' : ''
                  } has-text-centered`}
                  key={dayIndex}
                  onClick={this.openActivitiesListModal}
                >
                  {day > 0 ? formatDay(day) : ''}
                </td>
              ))}
            </tr>
          ))}
        </tbody>
      </table>
    );
  }

  render() {
    return (
      <div className="plan-container-wrapper columns">
        {this.displayActivitiesModal()}
        {this.displayCreateNewActivityModal()}
        <div className="plan-container columns is-multiline is-mobile">
          {this.displayBody()}
        </div>
      </div>
    );
  }
}

export default PlanContainer;
