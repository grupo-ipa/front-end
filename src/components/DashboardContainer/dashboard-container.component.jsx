import React from 'react';
import DashboardCards from './DashboardCards/dashboard-cards.component';
import './dashboard-container.styles.scss';
import DashboardActivityList from './DashboardActivityList/dashboard-activity-list.component';
import DashboardLastInfo from './DashboardLastInfo/dashboard-last-info.component';

class DashboardContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = { update: null };
  }

  update = () => this.setState({ update: new Date().toISOString() });

  displayBoard = () => {
    if (this.props.currentView === 'main') {
      return (
        <DashboardCards
          changeView={this.props.changeView}
          currentBarrel={this.props.currentBarrel}
        />
      );
    }
    return (
      <DashboardLastInfo
        changeView={this.props.changeView}
        currentBarrel={this.props.currentBarrel}
        attr={this.props.currentView}
        update={this.update}
      />
    );
  };

  render() {
    return (
      <section
        style={{ marginTop: '1.5em' }}
        className="section app-section columns"
      >
        <div className="column is-three-quarters">{this.displayBoard()}</div>
        <div className="column">
          <DashboardActivityList
            currentBarrel={this.props.currentBarrel}
            currentView={this.props.currentView}
            update={this.state.update}
          />
        </div>
      </section>
    );
  }
}

export default DashboardContainer;
