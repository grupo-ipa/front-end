import React from 'react';

import Spinner from '../../Spinner/spinner.component';

import { fetchLastUpdates } from '../../../services/dashboard.service';
import { convertToDashboardFormat } from '../../../utils/functions/date';

import './dashboard-activity-list.styles.scss';

class DashboardActivityList extends React.Component {
  constructor(props) {
    super(props);
    this.state = { lastUpdates: [], loading: true };
  }

  componentDidMount = async () => this.loadInfo();

  componentDidUpdate = async (prevProps) => {
    if (
      this.props.currentView !== prevProps.currentView ||
      this.props.update !== prevProps.update
    ) {
      await this.loadInfo();
    }
  };

  loadInfo = async () => {
    this.setState({ loading: true });
    const lastUpdates = await fetchLastUpdates();
    this.setState({ lastUpdates, loading: false });
  };

  render() {
    if (this.state.loading) return <Spinner />;
    return (
      <div className="dashboard-activity-list-wrapper hero">
        <div className="hero-head">
          <div className="dashboard-activity-list__head">Atividades</div>
        </div>
        <div className="hero-body">
          <div className="dashboard-activity-list__body">
            {this.state.lastUpdates.map(
              ({ id, nome, tanqueId, atributo, valor, createdAt }) => (
                <p key={id} className="dashboard-activity-list__info">
                  {`${nome || 'admin'} - `}
                  <span className="dashboard-activity-list__info--highlight">
                    {`tanque ${tanqueId}`}
                  </span>
                  {' - produto '}
                  <span className="dashboard-activity-list__info--highlight">
                    {`${atributo} ${valor}`}
                  </span>
                  <br />
                  <strong>{convertToDashboardFormat(createdAt)}</strong>
                </p>
              )
            )}
          </div>
        </div>
      </div>
    );
  }
}

export default DashboardActivityList;
