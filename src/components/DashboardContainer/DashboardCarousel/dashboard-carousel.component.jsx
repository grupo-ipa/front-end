import React from 'react';
import Item from './Item/item.component';
import './dashboard-carousel.styles.scss';
import { fetchBarrelsStatus } from '../../../services/dashboard.service';

//const tonelList = new Array(30).map((_, i) => i + 1);

class DashboardCarousel extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      items: this.props.items,
      active: this.props.active,
      direction: '',
    };
  }

  componentDidMount = async () => {
    this.setState({ loading: true });
    this.items = await fetchBarrelsStatus();
    this.setState({ loading: false });
  };

  generateItems() {
    let items = [];
    let level;
    for (let i = this.state.active - 2; i < this.state.active + 3; i++) {
      let index = i;
      if (i < 0) {
        index = this.state.items.length + i;
      } else if (i >= this.state.items.length) {
        index = i % this.state.items.length;
      }
      level = this.state.active - i;
      items.push(
        <Item key={index} id={this.state.items[index]} level={level} />
      );
    }
    return items;
  }

  moveLeft() {
    var newActive = this.state.active;
    newActive--;
    this.setState({
      active: newActive < 0 ? this.state.items.length - 1 : newActive,
      direction: 'left',
    });
  }

  moveRight() {
    var newActive = this.state.active;
    this.setState({
      active: (newActive + 1) % this.state.items.length,
      direction: 'right',
    });
  }

  render() {
    if (this.state.loading) return <p>loading...</p>;
    return (
      <div id="carousel" className="noselect">
        <div className="arrow arrow-left" onClick={this.leftClick}>
          <i className="fi-arrow-left"></i>
        </div>
        <>{this.generateItems()}</>
        <div className="arrow arrow-right" onClick={this.rightClick}>
          <i className="fi-arrow-right"></i>
        </div>
      </div>
    );
  }
}

export default DashboardCarousel;
