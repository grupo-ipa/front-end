import React from 'react';

import Spinner from '../../Spinner/spinner.component';
import Card from './Card/card.component';

import { barrelModel } from '../../../models/barrelInfo';
import { productModel } from '../../../models/product';
import {
  fetchNextActivities,
  fetchBarrelInfo,
} from '../../../services/dashboard.service';
import { fetchBeerInfo } from '../../../services/plan.service';
import { convertToNextActivitiesFormat } from '../../../utils/functions/date';

import './dashboard-cards.styles.scss';

class DashboardCards extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      data: null,
      nextActivities: [],
    };
  }

  componentDidMount = async () => {
    this.loadBarrelInfo();
  };

  componentDidUpdate = async (prevProps) => {
    if (this.props.currentBarrel !== prevProps.currentBarrel) {
      this.loadBarrelInfo();
    }
  };

  loadBarrelInfo = async () => {
    this.setState({
      loading: true,
      data: null,
      nextActivities: [],
    });
    let data = await fetchBarrelInfo(this.props.currentBarrel);
    if (data.produtoId) {
      const productData = await fetchBeerInfo(data.produtoId);
      const nextActivities = await fetchNextActivities(data.produtoId);
      data = { ...data, ...productData[0] };
      this.setState({
        nextActivities: nextActivities.slice(0, 2),
      });
    }
    this.setState({ data, loading: false });
  };

  handleClick = (property) => {
    if (this.state.data.produtoId) {
      this.props.changeView(property);
    }
  };

  displayBody() {
    if (this.state.loading) {
      return (
        <div className="is-fullwidth tank-table">
          <Spinner />
        </div>
      );
    }
    return (
      <>
        <div className="columns is-multiline dashboard-cards-wrapper">
          <div className="column is-one-third">
            <Card data={this.state.data} {...productModel} />
          </div>
          {barrelModel.map((barrel) => (
            <div
              key={barrel.property}
              className="column is-one-third"
              onClick={() => this.handleClick(barrel.property)}
            >
              <Card data={this.state.data} {...barrel} />
            </div>
          ))}
        </div>
        <div className="hero next-activities">
          <div className="hero-head">
            <div className="next-activities__head">
              <h2>Próximas Atividades</h2>
            </div>
          </div>
          <div className="hero-body">
            <ul className="next-activities__body">
              {this.state.nextActivities.map(
                ({ id, descricao, dataLimite }) => (
                  <li
                    key={id}
                  >{`${descricao} - PRAZO: ${convertToNextActivitiesFormat(
                    dataLimite
                  )}`}</li>
                )
              )}
            </ul>
          </div>
        </div>
      </>
    );
  }

  render() {
    return <div className="dashboard-table-wrapper">{this.displayBody()}</div>;
  }
}

export default DashboardCards;
