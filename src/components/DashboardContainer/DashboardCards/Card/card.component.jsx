import React from 'react';
import DashboardIcon from '../../../DashboardIcon/dashboard-icon.component';
import './card.styles.scss';

const units = Object.freeze({
  temperatura: '°C',
});

const Card = ({ title, icon, property, data }) => (
  <div className="dashboard-card">
    <h3>{title}</h3>
    <div className="dashboard-card-body">
      <DashboardIcon iconName={icon} source="main" />
    </div>
    <p>
      {data && (data[property] || data[property] === 0)
        ? `${data[property]}${units[property.toLowerCase()] || ''}`
        : '--------------'}
    </p>
  </div>
);

export default Card;
