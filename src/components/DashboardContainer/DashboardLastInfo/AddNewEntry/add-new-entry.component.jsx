import React from 'react';

import { addNewEntryToProp } from '../../../../services/dashboard.service';

class AddNewEntry extends React.Component {
  constructor(props) {
    super(props);
    this.state = { loading: false, value: '' };
  }

  handleChange = ({ target: { value } }) => this.setState({ value });

  handleSubmit = async (event) => {
    event.preventDefault();
    this.setState({ loading: true });
    await addNewEntryToProp(
      this.props.productId,
      this.props.tankId,
      this.props.attr,
      this.state.value
    );
    this.props.closeModal();
    this.props.update();
    this.setState({ loading: false });
  };

  render() {
    return (
      <div className="modal is-active">
        <div className="modal-background"></div>
        <div className="modal-card custom-card">
          <header className="modal-card-head">
            <p className="modal-card-title">Nova entrada</p>
            <button
              className="delete"
              aria-label="close"
              onClick={this.props.closeModal}
            ></button>
          </header>
          <section className="modal-card-body">
            <form noValidate>
              <div className="field is-horizontal">
                <div className="field-label is-normal">
                  <label className="label">Valor</label>
                </div>
                <div className="field-body">
                  <div className="field">
                    <p className="control">
                      <input
                        className="input"
                        type="text"
                        placeholder="Valor"
                        onChange={this.handleChange}
                      />
                    </p>
                  </div>
                </div>
              </div>

              <div className="field is-grouped is-grouped-centered">
                <button
                  className={`button is-success ${
                    this.state.loading ? 'is-loading' : ''
                  }`}
                  onClick={this.handleSubmit}
                >
                  Adicionar Valor
                </button>
              </div>
            </form>
          </section>
        </div>
      </div>
    );
  }
}

export default AddNewEntry;
