import React from 'react';

import Spinner from '../../Spinner/spinner.component';
import DashboardIcon from '../../DashboardIcon/dashboard-icon.component';
import AddNewEntry from './AddNewEntry/add-new-entry.component';

import { barrelModel } from '../../../models/barrelInfo';
import {
  fetchBarrelInfo,
  fetchAttrHistoryByProduct,
} from '../../../services/dashboard.service';
import { convertToDashboardFormat } from '../../../utils/functions/date';

import './dashboard-last-info.styles.scss';

const units = Object.freeze({
  temperatura: '°C',
});

class DashboardLastInfo extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      productId: null,
      unit: '',
      history: null,
      loading: true,
      modalOpened: false,
    };
  }

  openModal = () => this.setState({ modalOpened: true });
  closeModal = () => this.setState({ modalOpened: false });

  componentDidMount = async () => await this.loadLastInfo();

  loadLastInfo = async () => {
    this.setState({ productId: null, history: null, unit: '', loading: true });
    this.props.update();
    const barrelInfo = await fetchBarrelInfo(this.props.currentBarrel);
    const history = await fetchAttrHistoryByProduct(
      this.props.currentBarrel,
      this.props.attr
    );
    history.reverse();
    const unit = Object.keys(units).includes(this.props.attr.toLowerCase())
      ? units[this.props.attr.toLowerCase()]
      : '';
    this.setState({
      productId: barrelInfo.produtoId,
      history,
      loading: false,
      unit,
    });
  };

  iconName = () =>
    barrelModel.find(
      (el) => el.property.toLowerCase() === this.props.attr.toLowerCase()
    ).icon;

  render() {
    if (this.state.loading) return <Spinner />;
    const lastValue = this.state.history.length
      ? `${this.state.history[0].valor}${this.state.unit}`
      : '----------';
    return (
      <div className="dashboard-last-info-wrapper hero">
        {this.state.modalOpened && (
          <AddNewEntry
            productId={this.state.productId}
            tankId={this.props.currentBarrel}
            attr={this.props.attr}
            closeModal={this.closeModal}
            update={this.loadLastInfo}
          />
        )}
        <div className="hero-head columns dashboard-last-info-list__head">
          <div className="column is-4 is-offset-1 attr-info">
            <DashboardIcon iconName={this.iconName()} />
            <span>{this.props.attr}</span>
          </div>
          <div className="column is-4 is-offset-2 attr-info">{lastValue}</div>
        </div>
        <div className="hero-body">
          <ul className="dashboard-last-info-list__body">
            {this.state.history.map(({ id, valor, nome, createdAt }) => (
              <li key={id}>{`${valor}${this.state.unit} - ${
                nome || 'admin'
              }, ${convertToDashboardFormat(createdAt)}`}</li>
            ))}
          </ul>
        </div>
        <div className="dashboard-last-info-list__footer hero-footer">
          <button
            className="button is-default is-rounded"
            onClick={() => this.props.changeView('main')}
            style={{ marginRight: '10px' }}
          >
            Voltar
          </button>
          <button
            className="button is-default is-rounded"
            onClick={this.openModal}
          >
            + Novo Registro
          </button>
        </div>
      </div>
    );
  }
}

export default DashboardLastInfo;
