import React from 'react';
import './dashboard-icon.styles.scss';

const DashboardIcon = ({ iconName, source }) => {
  return (
    <img
      className="dashboard-icon"
      src={require(`../../assets/images/icons/${iconName}_${
        source === 'main' ? 'cinza' : 'branco'
      }.png`)}
      alt={iconName}
    />
  );
};

export default DashboardIcon;
