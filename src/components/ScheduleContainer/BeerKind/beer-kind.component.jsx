import React from 'react';
import './beer-kind.styles.scss';

//todo[vob] -> use Button as base
class BeerKind extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <div className="beer-kind-wrapper">
        <button
          className="button is-default"
          onClick={() => this.props.selectBeer(this.props.id, this.props.name)}
        >
          {this.props.name}
        </button>
      </div>
    );
  }
}

export default BeerKind;
