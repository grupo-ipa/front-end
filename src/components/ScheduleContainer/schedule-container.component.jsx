import React from 'react';

import BeerKind from './BeerKind/beer-kind.component';
import Spinner from '../Spinner/spinner.component';

import { fetchCurrentBeersInProduction } from '../../services/plan.service';

import './schedule-container.styles.scss';

class ScheduleContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = { loading: true, beerList: [] };
  }

  componentDidMount = async () => {
    this.setState({ loading: true });
    const products = await fetchCurrentBeersInProduction();
    this.setState({ beerList: products });
    this.setState({ loading: false });
  };

  appendNewBeer = (beer) =>
    this.setState({ beerList: this.state.beerList.concat([beer]) });

  render() {
    if (this.state.loading) {
      return <Spinner />;
    }
    return (
      <section className="section app-section">
        <div className="columns is-multiline is-mobile">
          {this.state.beerList.map(({ id, nome }) => (
            <div key={id} className="column is-half">
              <BeerKind
                id={id}
                name={nome}
                selectBeer={this.props.selectBeer}
              />
            </div>
          ))}
        </div>
      </section>
    );
  }
}

export default ScheduleContainer;
