import React from 'react';
import './button.styles.scss';

const Button = (props) => {
  const fn = props.selectBeer || props.fn;
  const param = props.id || props.param;
  return (
    <div className="button-wrapper">
      <button className="button is-default" onClick={() => fn(param)}>
        {props.text}
      </button>
    </div>
  );
};

export default Button;
