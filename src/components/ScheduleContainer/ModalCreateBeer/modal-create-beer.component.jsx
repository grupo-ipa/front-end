import React from 'react';
import { withRouter } from 'react-router-dom';

class ModalCreateBeer extends React.Component {
  constructor(props) {
    super(props);
    this.state = { name: '' };
  }

  handleChange = ({ target: { value } }) => this.setState({ name: value });

  handleSubmit = async (event) => {
    event.preventDefault();
    this.props.closeModal();
    this.props.history.push({
      pathname: '/selecionar-barril',
      state: { beerName: this.state.name },
    });
  };

  render() {
    return (
      <div className="modal is-active">
        <div className="modal-background"></div>
        <div className="modal-card custom-card">
          <header className="modal-card-head">
            <p className="modal-card-title">Cadastrar nova produção</p>
            <button
              className="delete"
              aria-label="close"
              onClick={this.props.closeModal}
            ></button>
          </header>
          <section className="modal-card-body">
            <form noValidate>
              <div className="field is-horizontal">
                <div className="field-label is-normal">
                  <label className="label">Nome</label>
                </div>
                <div className="field-body">
                  <div className="field">
                    <p className="control">
                      <input
                        className="input"
                        type="text"
                        placeholder="Nome da produção"
                        onChange={this.handleChange}
                      />
                    </p>
                  </div>
                </div>
              </div>

              <div className="field is-grouped is-grouped-centered">
                <button
                  className="button is-success"
                  onClick={this.handleSubmit}
                >
                  Criar
                </button>
              </div>
            </form>
          </section>
        </div>
      </div>
    );
  }
}

export default withRouter(ModalCreateBeer);
