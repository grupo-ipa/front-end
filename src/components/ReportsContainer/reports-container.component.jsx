import React from 'react';

import Spinner from '../Spinner/spinner.component';

import { fetchBarrelsStatus } from '../../services/dashboard.service';

class ReportsContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = { barrels: [], loading: true };
  }

  componentDidMount = async () => await this.loadBarrels();

  loadBarrels = async () => {
    this.setState({ loading: true });
    const barrels = await fetchBarrelsStatus();
    this.setState({ barrels, loading: false });
  };

  render() {
    if (this.state.loading) {
      return <Spinner />;
    }
    return (
      <section className="section app-section">
        <div className="columns is-multiline is-mobile">
          {this.state.barrels.map(({ id }) => (
            <div
              key={id}
              className="column is-one-quarter"
              style={{ marginTop: '1.5em' }}
            >
              <button
                className="button is-default is-barrel"
                style={{ width: '90%', padding: '1em' }}
              >
                {id}
              </button>
            </div>
          ))}
        </div>
      </section>
    );
  }
}

export default ReportsContainer;
