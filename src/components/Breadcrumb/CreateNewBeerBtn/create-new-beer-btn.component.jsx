import React from 'react';

import ModalCreateBeer from './ModalCreateBeer/modal-create-beer.component';

const CreateNewBeerBtn = () => {
  const [modal, setModal] = React.useState(false);
  const openModal = () => setModal(true);
  const closeModal = () => setModal(false);

  return (
    <>
      {modal && <ModalCreateBeer closeModal={closeModal} />}
      <button
        className="button is-default custom-breadcrumb-button"
        onClick={openModal}
      >
        + Adicionar Cerveja
      </button>
    </>
  );
};

export default CreateNewBeerBtn;
