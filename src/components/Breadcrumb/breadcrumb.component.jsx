import React from 'react';
import { Link, useLocation } from 'react-router-dom';
import CreateNewBeerBtn from './CreateNewBeerBtn/create-new-beer-btn.component';

import './breadcrumb.styles.scss';

const parser = (paths, state) => {
  let root = paths[0].toUpperCase();
  switch (root) {
    case 'CRONOGRAMA':
      return [root].concat(...((state && state.custom) || []));
    case 'DASHBOARD':
      return [root].concat([`Tanque ${paths[1]}`].concat(paths.slice(2)));
    default:
      return [root, ...paths.slice(1)];
  }
};

const specialComponent = (paths) => {
  let root = paths[0].toUpperCase();
  switch (root) {
    case 'CRONOGRAMA':
      return <CreateNewBeerBtn />;
    default:
      return;
  }
};

const Breadcrumb = (props) => {
  const location = useLocation();
  const paths = location.pathname.split('/').slice(1);
  const state = location.state;
  const crumbs = parser(paths, state);
  const numberOfCrumbs = crumbs.length - 1;
  const specialButton = specialComponent(paths);

  return (
    <section className="header">
      <nav
        className="breadcrumb has-arrow-separator custom-breadcrumb"
        aria-label="breadcrumbs"
      >
        <ul>
          {crumbs.map((crumb, i) =>
            i === numberOfCrumbs ? (
              <li key={i} className="is-active">
                <Link to={''} aria-current="page">
                  {crumb.toUpperCase()}
                </Link>
              </li>
            ) : (
              <li key={i}>
                <Link to={'/' + paths.slice(0, i + 1).join('/')}>
                  {crumb.toUpperCase()}
                </Link>
              </li>
            )
          )}
          {specialButton}
        </ul>
      </nav>
    </section>
  );
};
export default Breadcrumb;
