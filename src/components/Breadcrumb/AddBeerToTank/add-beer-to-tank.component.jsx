import React from 'react';
import { fetchBarrelInfo } from '../../../services/dashboard.service';
import { fetchCurrentBeersInProduction } from '../../../services/plan.service';

class AddBeerToTank extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      barrel: null,
      products: [],
      product: null,
      buttonLoading: null,
      modalOpened: false,
      selectedBeer: '',
    };
  }

  fetchProductsStates = async () => {
    const barrel = await fetchBarrelInfo(this.props.tank);
    const products = await fetchCurrentBeersInProduction();
    let product = null;
    if (barrel.produtoId) {
      product = products.find(({ id }) => id === barrel.produtoId);
    }
    this.setState({ barrel, products, product });
  };

  openModal = async () => {
    this.setState({ buttonLoading: 'is-loading' });
    await this.fetchProductsStates();
    this.setState({ modalOpened: true, buttonLoading: null });
  };

  closeModal = () => this.setState({ modalOpened: false });

  selectBeer = ({ target: { value } }) =>
    this.setState({ selectedBeer: value });

  modalBody = () => {
    if (!this.state.product) {
      return (
        <div className="select">
          <select onChange={this.selectBeer} value={this.state.selectedBeer}>
            <option>----------</option>
            {this.state.products.map((product) => (
              <option key={product.id}>{product.nome}</option>
            ))}
          </select>
        </div>
      );
    } else {
      return (
        <p>
          {`O tanque atualmente está ocupado com a cerveja ${this.state.product.nome}`}
        </p>
      );
    }
  };

  modalHtml = () => (
    <div className="modal is-active">
      <div className="modal-background"></div>
      <div className="modal-card custom-card">
        <header className="modal-card-head">
          <p className="modal-card-title">
            {`Status do tanque ${this.props.tank}: ${
              this.state.product ? 'Ocupado' : 'Livre'
            }`}
          </p>
          <button
            onClick={this.closeModal}
            className="delete"
            aria-label="close"
          ></button>
        </header>
        <section className="modal-card-body">{this.modalBody()}</section>
        <footer className="modal-card-foot is-centered">
          <button className="button is-default">
            Adicionar Nova Atividade
          </button>
        </footer>
      </div>
    </div>
  );

  render() {
    return (
      <>
        {this.state.modalOpened && this.modalHtml()}
        <button
          className={`button is-default custom-breadcrumb-button ${this.state.buttonLoading}`}
          onClick={this.openModal}
        >
          Adicionar cerveja a tanque
        </button>
      </>
    );
  }
}

export default AddBeerToTank;
