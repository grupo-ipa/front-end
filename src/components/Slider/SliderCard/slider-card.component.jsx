import React from 'react';

import tank from '../../../assets/images/icons/tonel-alt_cinza.png';
import tankActive from '../../../assets/images/icons/tonel-alt_branco.png';
import './slider-card.styles.scss';

const SliderCard = (props) => {
  return (
    <div
      className={`slider-card ${props.active ? 'slider-card__active' : ''}`}
      onClick={() => props.cardClicked(props.number)}
    >
      <span className="icon slider-icon is-large">
        <img src={props.active ? tankActive : tank} alt="" />
        <span className="slider-number">
          {props.number < 10 ? `0${props.number}` : props.number}
        </span>
      </span>
    </div>
  );
};

export default SliderCard;
