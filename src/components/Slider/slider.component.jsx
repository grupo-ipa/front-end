import React from 'react';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowLeft, faArrowRight } from '@fortawesome/free-solid-svg-icons';

import Spinner from '../Spinner/spinner.component';
import SliderCard from './SliderCard/slider-card.component';

import { fetchBarrelsStatus } from '../../services/dashboard.service';
import './slider.styles.scss';

class Slider extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      numberOfCards: 0,
      loading: true,
      currentLeft: 0,
      items: null,
    };
  }

  componentDidMount = async () => {
    this.setState({ loading: true });
    const items = await fetchBarrelsStatus();
    //todo: sort by tank number
    this.setState({
      items,
      loading: false,
      numberOfCards: Math.min(items.length, 4),
    });
  };

  cardClicked = (number) => {
    this.props.selectCard(number);
  };

  displayCards() {
    let cards = [];
    for (let i = 0; i < this.state.numberOfCards; ++i) {
      const number = this.state.items[this.state.currentLeft + i];
      cards.push(number);
    }
    return cards;
  }

  goLeft() {
    this.setState({ currentLeft: Math.max(0, this.state.currentLeft - 1) });
  }
  goRight() {
    this.setState({
      currentLeft: Math.min(
        Math.max(0, this.state.items.length - this.state.numberOfCards),
        this.state.currentLeft + 1
      ),
    });
  }

  render() {
    if (this.state.loading) return <Spinner />;
    return (
      <div className="slider-wrapper">
        <FontAwesomeIcon
          className="slider-control"
          icon={faArrowLeft}
          size="2x"
          onClick={() => this.goLeft()}
        />
        {this.displayCards().map((barrel) => (
          <SliderCard
            active={this.props.currentPage === barrel.id}
            number={barrel.id}
            key={barrel.id}
            cardClicked={this.cardClicked}
          />
        ))}
        <FontAwesomeIcon
          className="slider-control"
          icon={faArrowRight}
          size="2x"
          onClick={() => this.goRight()}
        />
      </div>
    );
  }
}

export default Slider;
