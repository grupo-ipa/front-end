import React from 'react';
import { login } from '../../utils/functions/auth';
import { mockAsync } from '../../utils/functions/fake-async';
import './login-form.styles.scss';
import logo from '../../assets/images/logo_escuro.png';
import { withRouter } from 'react-router-dom';

class LoginForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      username: '',
      password: '',
    };
  }

  componentDidMount = async () => {
    await mockAsync(1000);
    this.setState({ loading: false });
  };

  handleChange = (event) => {
    const id = event.target.id;
    this.setState({ [id]: event.target.value });
  };

  handleSubmit = async (event) => {
    this.setState({ loading: true });
    event.preventDefault();
    const { username, password } = this.state;
    await mockAsync(1500); // mocking async request to backend
    const userExists = username === 'admin' && password === 'password';
    if (userExists) {
      login(username);
      this.setState({ loading: false });
      this.props.history.push('/dashboard');
    } else {
      this.setState({ loading: false });
    }
  };

  render() {
    return (
      <section className="hero is-fullheight login-form-wrapper">
        <div className="hero-head">
          <img className="login-form--logo" src={logo} alt="" />
        </div>

        <div className="hero-body hero-body--vertical-stack margin-top--small">
          <form noValidate>
            <h2 className="title is-2 left-align">Login</h2>
            <div className="field">
              <label className="label">Nome de Usuário</label>
              <div className="control">
                <input
                  id="username"
                  className="input"
                  type="text"
                  placeholder="Seu login"
                  onChange={this.handleChange}
                />
              </div>
            </div>

            <div className="field">
              <label className="label">Senha</label>
              <div className="control">
                <input
                  id="password"
                  className="input"
                  type="password"
                  placeholder="Sua senha"
                  onChange={this.handleChange}
                />
              </div>
            </div>

            <div className="field">
              <div className="control">
                <button
                  className={`button is-default fullwidth ${
                    this.state.loading ? 'is-loading' : ''
                  }`}
                  onClick={this.handleSubmit}
                >
                  Entrar
                </button>
              </div>
            </div>
          </form>
        </div>

        <div className="hero-foot">
          <p className="right-align">versão 0.1</p>
        </div>
      </section>
    );
  }
}

export default withRouter(LoginForm);
