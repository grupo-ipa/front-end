export const barrelInfo = Object.freeze({
  produto: 'produto',
  status: 'status do tanque',
  dataProducao: 'data de produção',
  previsaoEnvase: 'previsão de envase',
  realEnvase: 'real envase',
  pesoPlato: 'peso plato',
  temperatura: 'temperatura',
  co2: 'CO2',
  ph: 'pH'
});

export const barrelModel = Object.freeze([
  {
    property: 'status',
    title: 'Status do Tanque',
    icon: 'status-tanque'
  },
  {
    property: 'dataProducao',
    title: 'Data de Produção',
    icon: 'data'
  },
  {
    property: 'previsaoEnvase',
    title: 'Previsão de Envase',
    icon: 'previsao-envase'
  },
  {
    property: 'realEnvase',
    title: 'Real Envase',
    icon: 'real-envase'
  },
  {
    property: 'peso',
    title: 'Peso Plato',
    icon: 'peso'
  },
  {
    property: 'temperatura',
    title: 'Temperatura',
    icon: 'temperatura'
  },
  {
    property: 'co2',
    title: 'CO2',
    icon: 'co2'
  },
  {
    property: 'ph',
    title: 'pH',
    icon: 'ph'
  }
]);