import React from 'react';
import { Switch, Route, BrowserRouter } from 'react-router-dom';
import LoginPage from './pages/public/login/login.page';
import PrivateContainerPage from './pages/private/private-container/private-container.page';
import './App.scss';

function App() {
  return (
    <BrowserRouter basename="/front-end">
      <Switch>
        <Route exact path="/login" component={LoginPage} />
        <Route component={PrivateContainerPage} />
      </Switch>
    </BrowserRouter>
  );
}

export default App;
