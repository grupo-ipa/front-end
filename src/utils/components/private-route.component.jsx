import React from 'react';
import { isAuthenticated } from '../functions/auth';
import { Route, Redirect } from 'react-router-dom';

const PrivateRoute = ({ component: Component, ...other }) => {
  return (
    <Route
      {...other}
      render={(props) =>
        isAuthenticated() ? (
          <Component {...props} />
        ) : (
          <Redirect
            to={{
              pathname: '/login',
              state: { from: props.location },
            }}
          />
        )
      }
    />
  );
};

export default PrivateRoute;
