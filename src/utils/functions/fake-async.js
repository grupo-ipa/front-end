export const mockAsync = async (time) => {
  return new Promise((resolve) => setTimeout(resolve, time));
}