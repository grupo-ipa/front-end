const formatToTwoDigits = (number) => `${number < 10 ? '0' : ''}${number}`;

export const formatDay = (day) => formatToTwoDigits(day);
export const formatMonth = (month) => formatToTwoDigits(month + 1);
export const formatDateToISO = (day, month, year) => {
  const formattedDay = typeof day === 'string' ? day : formatDay(day);
  const formattedMonth = typeof month === 'string' ? month : formatMonth(month);
  return `${year}-${formattedMonth}-${formattedDay}`;
}

export const convertToNextActivitiesFormat = (timestamp) => {
  const date = new Date(timestamp);
  return `${formatToTwoDigits(date.getDate())}/${formatToTwoDigits(date.getMonth() + 1)}/${date.getFullYear()}`;
}

export const convertToDashboardFormat = (timestamp) => {
  const date = new Date(timestamp);
  return `${formatToTwoDigits(date.getHours())}:${formatToTwoDigits(date.getMinutes())}, ${formatToTwoDigits(date.getDate())}/${formatToTwoDigits(date.getMonth() + 1)}/${date.getFullYear()}`;
}