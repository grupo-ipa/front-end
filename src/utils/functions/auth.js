import Cookies from 'js-cookie';
import { COOKIES_CONSTANTS } from '../constants/constants'

// [vob] methods to handle cookies agnostically.
export const isAuthenticated = () => Cookies.get(COOKIES_CONSTANTS.name);

export const login = (username) => Cookies.set(COOKIES_CONSTANTS.name, username, { expires: COOKIES_CONSTANTS.expirationTime });

export const logout = () => Cookies.remove(COOKIES_CONSTANTS.name);
