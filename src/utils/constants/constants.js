export const COOKIES_CONSTANTS = Object.freeze({
  name: 'tulipa_username',
  expirationTime: 1 // 1 day
});

export const DATE_FORMAT = 'YYYY-MM-DD';