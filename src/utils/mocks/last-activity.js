export const lastActivity = [
  {
    id: 32,
    data: {
      author: 'Maria Clara',
      barrel: 1,
      change: {
        property: 'Temperatura',
        value: -2
      },
      date: "2020-05-24T21:28:37.058Z"
    }
  },
  {
    id: 33,
    data: {
      author: 'João Antônio',
      barrel: 3,
      change: {
        property: 'Temperatura',
        value: -5
      },
      date: "2020-05-24T21:26:30.058Z"
    }
  },
  {
    id: 34,
    data: {
      author: 'João Antônio',
      barrel: 1,
      change: {
        property: 'Temperatura',
        value: -2
      },
      date: "2020-05-24T21:25:37.058Z"
    }
  },

  {
    id: 35,
    data: {
      author: 'João Antônio',
      barrel: 5,
      change: {
        property: 'Temperatura',
        value: -5
      },
      date: "2020-05-23T23:25:37.058Z"
    }
  },
]