export const mockBeerKind = [
  {
    id: 1,
    nome: 'Premium Lager',
  },
  {
    id: 2,
    nome: 'Munich Helles',
  },
  {
    id: 3,
    nome: 'Session IPA El Chancho',
  },
  {
    id: 4,
    nome: 'APA 1817',
  },
  {
    id: 5,
    nome: 'American IPA',
  },
  {
    id: 6,
    nome: 'Czech Pilsener',
  },
  {
    id: 7,
    nome: 'Sour Cajá',
  },
  {
    id: 8,
    nome: 'Hopbeat New England IPA',
  },
  {
    id: 9,
    nome: 'Extra Stout',
  },
  {
    id: 10,
    nome: 'Coffee Stout',
  },
  {
    id: 11,
    nome: 'Batalha de Pirajá',
  },
];