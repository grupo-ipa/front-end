export const plannedActivities = Object.freeze([
  {
    id: 1,
    data: "2020-06-14",
    atividades: [
      {
        id: 23,
        tarefa: "Reunião de planejamento",
        dataLimite: "2020-06-14",
        tarefaConcluida: true
      },
      {
        id: 40,
        tarefa: "Iniciar processo",
        dataLimite: "2020-06-14",
        tarefaConcluida: true
      },
      {
        id: 47,
        tarefa: "Estimar datas",
        dataLimite: "2020-06-15",
        tarefaConcluida: false
      }
    ]
  },
  {
    id: 1,
    data: "2020-06-15",
    atividades: [
      {
        id: 56,
        tarefa: "Monitoramento",
        dataLimite: "2020-06-15",
        tarefaConcluida: true
      },
    ]
  },
  {
    id: 1,
    data: "2020-06-16",
    atividades: []
  },
  {
    id: 1,
    data: "2020-06-17",
    atividades: [
      {
        id: 58,
        tarefa: "Adicionar água à mistura",
        dataLimite: "2020-06-17",
        tarefaConcluida: false
      },
      {
        id: 61,
        tarefa: "Deixar resfriar",
        dataLimite: "2020-06-18",
        tarefaConcluida: false
      },
    ]
  },

]);