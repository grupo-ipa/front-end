// API Call: /barrels

export const barrelsStatus = Object.freeze([
  { id: 1, data: { status: 'parado' } },
  { id: 2, data: { status: 'parado' } },
  { id: 3, data: { status: 'parado' } },
  { id: 4, data: { status: 'parado' } },
  { id: 5, data: { status: 'parado' } },
  { id: 6, data: { status: 'parado' } },
  { id: 7, data: { status: 'parado' } },
  { id: 8, data: { status: 'fermentando' } },
  { id: 9, data: { status: 'parado' } },
  { id: 10, data: { status: 'desativado' } },
  { id: 11, data: { status: 'desativado' } },
  { id: 12, data: { status: 'parado' } },
  { id: 13, data: { status: 'desativado' } },
  { id: 14, data: { status: 'parado' } }
]);

// API Call: /barrels/:id

export const barrels = [
  { id: 1, dados: { status: 'parado' } },
  { id: 2, dados: { status: 'parado' } },
  { id: 3, dados: { status: 'parado' } },
  { id: 4, dados: { status: 'parado' } },
  { id: 5, dados: { status: 'parado' } },
  { id: 6, dados: { status: 'parado' } },
  { id: 7, dados: { status: 'parado' } },
  {
    id: 8,
    dados: {
      produto: 'IPA',
      status: 'fermentando',
      dataProducao: '19/05/2020',
      previsaoEnvase: '20/06/2020',
      realEnvase: '21/06/2020',
      pesoPlato: '0000',
      temperatura: '55 / 60',
      co2: '0000',
      ph: '0000',
    }
  },
  { id: 9, dados: { status: 'parado' } },
  { id: 10, dados: { status: 'desativado' } },
  { id: 11, dados: { status: 'desativado' } },
  { id: 12, dados: { status: 'parado' } },
  { id: 13, dados: { status: 'desativado' } },
  { id: 14, dados: { status: 'parado' } }
];

// status can be: 'fermentando/other work...', 'parado' or 'desativado'